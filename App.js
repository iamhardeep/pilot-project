import React, { Component } from 'react';
// import {StyleSheet, Text, View} from 'react-native';
import MainContainer from './maincontainer'
import {Provider} from 'react-redux';
import {store} from './store';





export default class App extends Component {
  
  render() {
    return (
      <Provider store={store}>
        <MainContainer/>
      </Provider>
    );
  }
}



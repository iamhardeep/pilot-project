import React, { Component } from 'react';
import {
  Container,
  Header,
  Content,
  Footer,
  Button,
  Text,
  Form,
  Item,
  Input,
  Label,
} from 'native-base';
import {connect} from 'react-redux';
import {addtext} from './action/index';
import PushNotification from 'react-native-push-notification';

class MainContainer extends Component{
    constructor(props){
        super(props);
        this.state={
            text:''
        }
        this.sendNotification = this.sendNotification.bind(this);
    }
    sendNotification() {
        console.log(this.props)
        // this.props.pushnotification(this.state.text),
          PushNotification.localNotification({
          message:this.props.text
        });
      };

    render()
    {
        return(
          <Container>
            <Header />
            <Content padder>
            
              <Button primary onPress={this.sendNotification}><Text> Notification </Text></Button>
              {/* <Button primary onPress={()=>this.props.pushnotification(this.state.text)}><Text> Store </Text></Button> */}
              {/* <Text>{this.props.text}</Text>             */}
            </Content>
            <Footer />
          </Container>
        )
    }
  }
  function mapStateToPorps(state){
    //   console.log(JSON.stringify(state))
    return{
      text:state
    }
  }
  function mapDispatchToProps(dispatch){
    return {
      pushnotification:(text)=>{dispatch(addtext(text))}
    }
  }
  export default connect(mapStateToPorps,mapDispatchToProps)(MainContainer)